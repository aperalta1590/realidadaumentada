﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Jugar : MonoBehaviour {
	// Update is called once per frame
	void ChangeToScene (string sceneToChangeTo) {
        SceneManager.LoadScene(sceneToChangeTo);
	}
}
