﻿using UnityEngine;
using System.Collections;

public class administradorBotones : MonoBehaviour {
    //este código abre la escena del juego
    public void botonJugar()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("flag_Espana");
    }
    //este código abre la escena del mapa
    public void botonMapa()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }
    //este cierra la aplicación
    public void botonSalir()
    {
        Application.Quit();
    }}
