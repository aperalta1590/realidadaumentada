﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public static class CountryManager{
    public static Dictionary<string, CountryData> countries = new Dictionary<string, CountryData>();
    public static void Populate()
    {
        CountryData data = new CountryData();
        //Europa
        
        data.nombrePais="España";
        data.capital = "Madrid";
        data.moneda = "Euro";
        data.poblacion = "46'468.102 habitantes";
        data.idioma = "Castellano";
        data.idHimno = 0;
        countries.Add("Spain",data);
        //América
        data = new CountryData();
        data.nombrePais = "México";
        data.capital = "Ciudad de México";
        data.moneda = "Peso Mexicano";
        data.poblacion = "46'468.102 habitantes";
        data.idioma = "Español Mexicano";
        data.idHimno = 1;
        countries.Add("Mexico", data);
    }

}
