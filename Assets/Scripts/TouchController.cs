﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
public class TouchController : MonoBehaviour {
    private Ray ray;
    private RaycastHit hit;
    private Touch touch;
    public Camera cam;
    public LayerMask mask;
    AudioSource Himno;
    int band = -1;
    string pathHimno;
    public Image MusicProgress;
    AudioClip HimnoSound;

    public static string pais;

    public GameObject Panel;

    public Text lblPaisValue;
    public Text lblCapitalValue;
    public Text lblMonedaValue;
    public Text lblPoblacionValue;
    public Text lblIdiomaValue;

    public Image BanderaPais;
    public AudioClip[] clips;

    public bool isPlaying=false;

    void Start()
    {
        CountryManager.Populate();
        
        Himno = gameObject.AddComponent<AudioSource>() as AudioSource;
        
    }
    void Update()
    {
        if (Panel.activeSelf == false)
        {
            MusicProgress.fillAmount = 0;

#if UNITY_EDITOR || UNITY_STANDALONE

            if (Input.GetMouseButtonUp(0))
            {
                ray = cam.ScreenPointToRay(Input.mousePosition);
                CastRay();
            }
#endif
#if UNITY_ANDROID
                    if (Input.touchCount > 0)
                    {
                        touch = Input.GetTouch(0);
                        if (touch.phase != TouchPhase.Ended && touch.phase != TouchPhase.Canceled)
                        {
                            ray = cam.ScreenPointToRay(touch.position);
                            CastRay();
                        }
                    }
#endif
        }
        if (isPlaying)
        {
            if (band == 0)
            {
                Himno.clip = HimnoSound;
                Himno.Play();
            }
            else
            {
                Himno.PlayDelayed(44100);
            }
            MusicProgress.fillAmount += (1 / HimnoSound.length) * Time.deltaTime;
            if (MusicProgress.fillAmount==1)
            {
                Himno.Stop();
            }
            
        }else
        {
            if (Panel.activeSelf)
            {
                Himno.Pause();
            }
        }

        
    }
    
    private void CastRay()
    {
        if (Physics.Raycast(ray, out hit, mask))
        {
            GameObject go = hit.transform.gameObject;
            isPlaying = false;
            Debug.Log(go.name);
            ShowData(go.name);
        }
    }
    private void ShowData(string key)
    {
        CountryData temp = CountryManager.countries[key];
        lblPaisValue.text = temp.nombrePais;
        lblCapitalValue.text = temp.capital;
        lblIdiomaValue.text = temp.idioma;
        lblMonedaValue.text = temp.moneda;
        lblPoblacionValue.text = temp.poblacion;
        pathHimno = "Himnos/" + key;
        BanderaPais.sprite = Resources.Load<Sprite>("Imagenes/" + key);
        HimnoSound = clips[temp.idHimno];
        
        Panel.SetActive(true);
    }
    public void MusicControl()
    {
        Debug.Log(isPlaying);
        if (band == -1) { band++; }
        isPlaying = !isPlaying;

    }
}
