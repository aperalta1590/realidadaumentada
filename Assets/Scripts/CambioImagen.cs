﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class CambioImagen : MonoBehaviour {
    public Image UIImagen;
    public void Start()
    {
        UIImagen = GameObject.Find("ImagenCambiante").GetComponent<Image>();
        UIImagen.sprite = Resources.Load <Sprite>("Texturas/principal");
    }
    public void Update()
    {
        UIImagen.sprite = Resources.Load<Sprite>("Texturas/principal");
    } 
}
